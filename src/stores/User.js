import { observable, action } from 'mobx';

const STARTING_BALANCE = 3000;
class User {

  @observable balance;
  @observable stocks = [];

  constructor() {
    if (!localStorage.hasOwnProperty('balance')) {
      localStorage.setItem('balance', STARTING_BALANCE);
    }
  }

  @action
  refresh() {
    this.balance = parseFloat(localStorage.getItem('balance'));
    this.stocks = JSON.parse(localStorage.getItem('stocks')) || [];
  }

  @action
  withdraw(amount) {
    this.balance -= amount;
    localStorage.setItem('balance', this.balance);
  }

  @action
  deposit(amount) {
    this.balance += amount;
    localStorage.setItem('balance', this.balance);
  }

  @action
  addToStocks(symbol, quantity) {
    let newStock = {
      symbol: '',
      quantity: 0,
    };
    let existing = this.stocks.find(obj => obj.symbol === symbol);
    if (existing) {
      existing.quantity = parseInt(existing.quantity) + quantity;
      newStock = existing;
    } else {
      newStock.symbol = symbol;
      newStock.quantity = quantity;
      this.stocks.push(newStock);
    }
    localStorage.setItem('stocks', JSON.stringify(this.stocks));
  }

  @action
  removeFromStocks(symbol, quantity) {
    let i = this.stocks.findIndex(obj => obj.symbol === symbol);
    if (i !== -1) {
      this.stocks[i].quantity -= quantity;
      localStorage.setItem('stocks', JSON.stringify(this.stocks));
    }
  }
}

export default new User();
