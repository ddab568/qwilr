import { observable, action } from 'mobx';

const BASE_URL = 'https://www.alphavantage.co/query?';
const API_KEY = 'QGF0RDNYGX3RQM2U';
const INTERVAL = '5min';
const SEARCH_LABELS = {
  symbol: '1. symbol',
  name: '2. name',
  type: '3. type',
  region: '4. region',
  marketOpen: '5. marketOpen',
  marketClose: '6. marketClose',
  timezone: '7. timezone',
  currency: '8. currency',
  matchScore: '9. matchScore',
};

const TIMESERIES_LABELS = {
  open: '1. open',
  high: '2. high',
  low: '3. low',
  close: '4. close',
  volume: '5. volume',
};

class Stock {
  @observable price;
  @observable timeSeries;

  symbolSearch = async (keyword) => {
    let response = await fetch(
      `${BASE_URL}function=SYMBOL_SEARCH&keywords=${keyword}&apikey=${API_KEY}`
    );
    let result = await response.json();
    let matches = [];
    if (result.bestMatches) {
      matches = result.bestMatches.map((match) => {
        //Format for ease of use
        match['symbol'] = match[SEARCH_LABELS.symbol];
        delete match['1. symbol'];
        return match;
      });
    }
    return matches;
  };

  initStock = async (symbol) => {
    let response = await fetch(
      `${BASE_URL}function=TIME_SERIES_INTRADAY&interval=${INTERVAL}&symbol=${symbol}&apikey=${API_KEY}`
    );
    let result = await response.json();
    let data = result['Time Series (5min)'];
    this.updatePrice(data);
    this.updateTimeSeries(data);
  };

  @action
  updatePrice = (timeSeries) => {
    const lastTimeStamp = Object.keys(timeSeries)[0];
    this.price = timeSeries[lastTimeStamp][TIMESERIES_LABELS.close];
  };

  @action
  updateTimeSeries = (data) => {
    let updatedTimeSeries = {
      labels: [],
      data: [],
    };
    Object.keys(data).forEach(function (key) {
      updatedTimeSeries['labels'].push(new Date(key).toLocaleTimeString());
      updatedTimeSeries['data'].push(
        parseFloat(data[key][TIMESERIES_LABELS.close])
      );
    });
    this.timeSeries = updatedTimeSeries;
  };
}

export default new Stock();
