import React from 'react';
import NumberFormat from 'react-number-format';

export function formatCurrency(amount) {
  return (
    <NumberFormat
      value={amount}
      displayType={'text'}
      thousandSeparator={true}
      decimalScale={2}
      prefix={'$'}
    />);
}