import React, { Component } from 'react';
import { Line } from 'react-chartjs-2';
import Typography from '@material-ui/core/Typography'
import { inject, observer } from 'mobx-react';

const data = {
  labels: [],
  datasets: [
    {
      label: 'Intraday Price',
      fill: false,
      lineTension: 0.1,
      backgroundColor: 'rgba(75,192,192,0.4)',
      borderColor: 'rgba(75,192,192,1)',
      borderCapStyle: 'butt',
      borderDash: [],
      borderDashOffset: 0.0,
      borderJoinStyle: 'miter',
      pointBorderColor: 'rgba(75,192,192,1)',
      pointBackgroundColor: '#fff',
      pointBorderWidth: 1,
      pointHoverRadius: 5,
      pointHoverBackgroundColor: 'rgba(75,192,192,1)',
      pointHoverBorderColor: 'rgba(220,220,220,1)',
      pointHoverBorderWidth: 2,
      pointRadius: 1,
      pointHitRadius: 10,
      data: [],
    }
  ]
};
@inject('stock')
@observer
class StockChart extends Component {
  render() {
    let { title, stock } = this.props;
    let { timeSeries } = stock;
    if (timeSeries) {
      data.labels = timeSeries.labels.slice(0, 10).reverse();
      data.datasets[0].data = timeSeries.data.slice(0, 10).reverse();
    }
    return (
      <div>
        <Typography variant='h5'>
          {title}
        </Typography>
        {timeSeries &&
          <Line data={data} />
        }
      </div>
    );
  };
}

export default StockChart;