import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Input from '@material-ui/core/Input';
import InputAdornment from '@material-ui/core/InputAdornment';

export default class FormDialog extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      inputValue: ''
    };
  }

  updateInputValue = (e) => {
    this.setState({
      inputValue: e.target.value
    });
  }

  render() {
    const {
      open,
      title,
      handleClose,
      handleConfirm,
    } = this.props;

    const { inputValue } = this.state;
    return (
      <Dialog
        open={open}
        onClose={handleClose}
      >
        <DialogTitle>{title}</DialogTitle>
        <DialogContent>
          <Input
            value={inputValue}
            autoFocus
            margin="dense"
            type="number"
            fullWidth
            startAdornment={
              <InputAdornment position="start">$</InputAdornment>
            }
            onChange={this.updateInputValue}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Cancel
            </Button>
          <Button onClick={(e) => handleConfirm(inputValue)} color="primary">
            {title}
          </Button>
        </DialogActions>
      </Dialog>
    );
  }
}