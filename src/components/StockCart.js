import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import { formatCurrency } from '../utils';
import { withStyles } from '@material-ui/core/styles';

const styles = {
  root: {
    padding: 10,
  },
};

@inject('user', 'stock')
@observer
class StockCart extends Component {

  constructor() {
    super();
    this.state = {
      quantity: 0,
      totalPrice: 0,
    }
  }

  updateQuantity = (e) => {
    const quantity = e.target.value;
    this.setState({
      quantity: quantity,
      totalPrice: quantity * this.props.stock.price,
    });
  }

  handleBuy = (e) => {
    this.props.user.withdraw(this.state.totalPrice)
    this.props.user.addToStocks(
      this.props.symbol,
      parseInt(this.state.quantity),
    );
  }

  handleSell = (e) => {
    this.props.user.deposit(this.state.totalPrice);
    this.props.user.removeFromStocks(
      this.props.symbol,
      parseInt(this.state.quantity),
    )
  }

  isSell = (e) => {
    let urlParameters = window.location.href.split('/');
    let value = urlParameters[urlParameters.length - 1];
    return value === 'sell';
  }

  render() {
    const { classes, symbol, stock} = this.props;
    const { price } = stock;
    const {
      quantity,
      totalPrice,
    } = this.state;

    const sell = this.isSell()
    const handleTransaction = sell ? this.handleSell : this.handleBuy;
    return (
      <Paper className={classes.root}>
        <Typography variant="h5" gutterBottom>
          {sell ? 'Sell' : 'Buy'}
        </Typography>
        <Grid container spacing={16} alignItems="center">
          <Grid item xs={4}>
            <strong>{symbol}</strong>
          </Grid>
          <Grid item xs={4}>
            {formatCurrency(price)}
          </Grid>
          <Grid item xs={4}>
            <TextField
              type="number"
              label="Amount"
              value={quantity}
              onChange={this.updateQuantity}
              margin="normal"
              variant="outlined"
            />
          </Grid>
          <Grid item xs={6}>
            Total: {formatCurrency(totalPrice)}
          </Grid>
          <Grid item xs={6}>
            <Button
              variant="contained"
              color="primary"
              onClick={handleTransaction}
              style={{ float: 'right' }}
            >
              {sell ? 'Sell' : 'Buy'}
            </Button>
          </Grid>
        </Grid>
      </Paper>
    );
  }

};

export default withStyles(styles)(StockCart);