import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import Divider from '@material-ui/core/Divider';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import { formatCurrency } from '../utils';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import List from '@material-ui/core/List';
import Dialog from '../components/BalanceDialog';
import { Link } from 'react-router-dom';
import AccountCircle from '@material-ui/icons/AccountCircle';

const drawerWidth = 240;

const styles = theme => ({
  profile: {
    display: 'inline-block',
    verticalAlign: 'middle'
  },
  profileHeading: {
    marginLeft: 3,
  },
  drawer: {
    width: drawerWidth,
  },
  stockSection: {
    marginTop: 20,
  },
  content: {
    marginTop: 80,
    paddingLeft: 20,
  },
  drawerPaper: {
    width: drawerWidth,
  },
  toolbar: theme.mixins.toolbar,
  headings: {
    marginLeft: 10,
    marginTop: 10,
  }
});

@inject('user')
@observer
class ProfileView extends Component {
  constructor() {
    super();
    this.state = {
      openDialog: false,
      dialogTitle: '',
    }
  }

  componentDidMount() {
    this.props.user.refresh();
  }

  openDialog = (title) => {
    this.setState({
      openDialog: true,
      dialogTitle: title,
    });
  }

  closeDialog = () => {
    this.setState({ openDialog: false });
  }

  handleBalance = (amount) => {
    amount = parseFloat(amount) || 0;
    if (this.state.dialogTitle === 'Withdraw') {
      this.props.user.withdraw(amount);
    } else {
      this.props.user.deposit(amount);
    }
  }

  render() {
    const { classes, user } = this.props;
    const { openDialog, dialogTitle } = this.state;
    return (
      <Drawer
        className={classes.drawer}
        variant="permanent"
        classes={{
          paper: classes.drawerPaper,
        }}
      >
        <div className={classes.content} >
          <AccountCircle
            color='action'
            className={classes.profile}
          />
          <Typography
            variant="h5"
            className={classes.profile}
          >
            Profile
          </Typography>
          <List>
            <ListItem>
              <ListItemText
                primary={formatCurrency(user.balance)}
                secondary={'balance'}
              />
            </ListItem>
          </List>
        </div>
        <div style={{ textAlign: 'center' }}>
          <Button
            color="primary"
            variant="contained"
            onClick={() => this.openDialog('Deposit')}
          >
            Deposit
          </Button>
          <Button
            color="secondary"
            variant="contained"
            onClick={() => this.openDialog('Withdraw')}
          >
            Withdraw
          </Button>
        </div>
        <Divider className={classes.stockSection} />
        <Typography variant='h6' className={classes.headings}>
          Purchased Stock
        </Typography>
        <List>
          {user.stocks.map(({ symbol, quantity }) =>
            <ListItem key={symbol}>
              <ListItemText primary={symbol} secondary={'x' + quantity} />
              <Button
                color="primary"
                variant="outlined"
                component={Link}
                to={`/stock/${symbol}/sell`}
              >
                Sell
              </Button>
            </ListItem>
          )}
        </List>
        <Dialog
          title={dialogTitle}
          open={openDialog}
          handleClose={this.closeDialog}
          handleConfirm={this.handleBalance}
        />
      </Drawer>
    );
  }
}

ProfileView.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ProfileView);