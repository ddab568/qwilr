import React, { Component } from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import SearchAppBar from '../components/SearchAppBar';
import ProfileView from './ProfileView';
import AppRouter from '../routers/AppRouter';
import { withStyles } from '@material-ui/core/styles';

const style = (theme) => ({
  root: {
    ...theme.typography,
  },
});

class App extends Component {
  render() {
    const {classes} = this.props;
    return (
      <div className={classes.root}>
        <CssBaseline/>
        <SearchAppBar/>
        <ProfileView />
        <AppRouter/>
      </div>
    );
  }
}

export default withStyles(style)(App);
