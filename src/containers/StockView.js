import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import StockCart from '../components/StockCart';
import StockChart from '../components/StockChart';
import { withStyles } from '@material-ui/core/styles';

const styles = {
  root: {
    width: '80%',
    margin: '100px 250px',
  },
  item: {
    padding: 10,
  },
};
@inject('user', 'stock')
@observer
class StockView extends Component {
  componentDidMount() {
    const { symbol } = this.props.match.params;
    this.props.stock.initStock(symbol);
    this.props.user.refresh();
  }

  render() {
    const { classes, match } = this.props;
    const { symbol } = match.params;
    return (
      <Grid className={classes.root} container justify="center" spacing={16}>
        <Grid item xs={8}>
          <Paper className={classes.item}>
            <StockChart title={symbol} />
          </Paper>
        </Grid>
        <Grid item xs={4}>
          <StockCart symbol={symbol} />
        </Grid>
      </Grid>
    );
  }
}

export default withStyles(styles)(StockView);
