import React from 'react';
import { Switch, Route } from 'react-router-dom';
import StockView from './../containers/StockView';

class AppRouter extends React.Component {
  render() {
    return (
      <Switch>
        <Route path="/stock/:symbol" component={StockView} />
        <Route path="/stock/:symbol/sell" component={StockView} />
      </Switch>
    );
  }
}

export default AppRouter;
